#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_BMP085.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Gyver433.h>
#include <microDS18B20.h>

// Настройки приемника
#define RADIO_DATA 13
#define RADIO_BUF_SIZE 10
#define G433_SPEED 1000

// Общие настройки
// WiFi-settings
const char *ssid = "***";
const char *password = "***";

// MQTT-settings
const char *mqtt_server = "***";
const char *mqtt_login = "***";
const char *mqtt_password = "***";
const char *mqtt_clientId = "weatherstation";

// Создаем объект-киента WiFi и MQTT
WiFiClient espClient;
PubSubClient client(espClient);

// Инициализация сенсоров базовой станции.
Gyver433_RX<RADIO_DATA, RADIO_BUF_SIZE, G433_XOR> rx;
MicroDS18B20<0> ds;
Adafruit_SSD1306 oled(0);
Adafruit_BMP085 bmp;

// Таймеры
unsigned long lastScreenUpdateTimer = 0;
unsigned long lastPredictionUpdateTimer = 0;
unsigned long lastMqttMsgTimer = 0;

// Параметры
double temperatureInside = 0;
double temperatureOutside = 0;
unsigned long pressureInside = 0;

// Всякое для предсказывания погоды
unsigned long pressure, aver_pressure, pressure_array[6], time_array[6];
unsigned long sumX, sumY, sumX2, sumXY;
int delta;
float a, b;

void setup()
{
  pinMode(BUILTIN_LED, OUTPUT);
  Serial.begin(115200);

  setup_wifi(); //  Настройка WiFi подключения

  client.setServer(mqtt_server, 1883); // Задаем адрес сервера MQTT, но пока не подключаемся

  oled.begin(SSD1306_SWITCHCAPVCC);
  oled.display();

  bmp.begin();

  // Инициализация массива для предсказателя погоды
  pressure = aver_sens();
  for (byte i = 0; i < 6; i++)
  {
    pressure_array[i] = pressure;
    time_array[i] = i;
  }
}

void loop()
{
  if (!client.connected())
  {
    mqtt_reconnnect();
  }

  client.loop();

  // Работа с внешним датчиком (постоянно в лупе)
  if (rx.tickWait())
  {
    // Сообщение от внешнего модуля 0x01
    if (rx.buffer[1] == 0xA5 && rx.buffer[0] == 0x01)
    {
      // Парсим данные
      temperatureOutside = ds.calcRaw(rx.buffer[2] << 8 | rx.buffer[3]);
      // Отправляем сообщение по топику как только получаем инфу от внешнего датчика
      client.publish("home/weatherstation/outside/temperature", String(temperatureOutside).c_str());
    }
  }

  unsigned long now = millis();
  if (now - lastScreenUpdateTimer > 1500)
  {
    // Каждые несколько секунд измеряем температуру внутри
    temperatureInside = bmp.readTemperature();
    pressureInside = bmp.readPressure();
    updateDisplay();
    lastScreenUpdateTimer = now;
  }

  if (now - lastMqttMsgTimer > 10000)
  {
    lastMqttMsgTimer = now;
    // Отправляем данные раз в 10 секунд.
    client.publish("home/weatherstation/inside/temperature", String(temperatureInside).c_str());
    client.publish("home/weatherstation/inside/pressure", String(pressure).c_str());
  }

  // Предсказание погоды
  if (now - lastPredictionUpdateTimer > 10 * 1000 * 60)
  {
    lastPredictionUpdateTimer = now;
    int pred_value = predict_weather();

    String type;
    if (pred_value < -150)
    {
      type = "Very bad weather";
    }
    else if (pred_value < -50)
    {
      type = "Bad weather";
    }
    else if (pred_value > 50)
    {
      type = "Good weather";
    }
    else if (pred_value >= 50)
    {
      type = "Very good weather";
    }
    else
    {
      type = "No change";
    }

    client.publish("home/weatherstation/prediction/value", String(pred_value).c_str());
    client.publish("home/weatherstation/prediction/type", String(type).c_str());
  }
}

// Настройка WiFi подключения
void setup_wifi()
{
  delay(10);

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA); // Установка типа шифрования
  WiFi.begin(ssid, password);
  // WiFi.begin(ssid); // Меняем строчку выше на эту, если пароля нет

  // Ждем пока не подключится
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  // Выводим инфу о подключении
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Подключение к MQTT
void mqtt_reconnnect()
{
  // Пока не подключится, будем пытаться это сделть
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");

    if (client.connect(mqtt_clientId, mqtt_login, mqtt_password))
    {
      Serial.println("Mqtt connected");
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void updateDisplay()
{
  oled.clearDisplay();
  oled.setTextSize(1);
  oled.setTextColor(WHITE);

  oled.setCursor(0, 0);

  oled.println();
  oled.print(temperatureInside);
  oled.print(" *C ");
  oled.print(pressure);
  oled.println("/ Pa");

  oled.print(temperatureOutside);
  oled.println(" *C");

  oled.display();
}

int predict_weather()
{
  pressure = aver_sens(); // найти текущее давление по среднему арифметическому
  for (byte i = 0; i < 5; i++)
  {                                            // счётчик от 0 до 5 (да, до 5. Так как 4 меньше 5)
    pressure_array[i] = pressure_array[i + 1]; // сдвинуть массив давлений КРОМЕ ПОСЛЕДНЕЙ ЯЧЕЙКИ на шаг назад
  }
  pressure_array[5] = pressure; // последний элемент массива теперь - новое давление
  Serial.println(pressure);
  sumX = 0;
  sumY = 0;
  sumX2 = 0;
  sumXY = 0;
  for (int i = 0; i < 6; i++)
  { // для всех элементов массива
    sumX += time_array[i];
    sumY += (long)pressure_array[i];
    sumX2 += time_array[i] * time_array[i];
    sumXY += (long)time_array[i] * pressure_array[i];
  }
  a = 0;
  a = (long)6 * sumXY; // расчёт коэффициента наклона прямой
  a = a - (long)sumX * sumY;
  a = (float)a / (6 * sumX2 - sumX * sumX);
  delta = a * 6; // расчёт изменения давления
  Serial.println(delta);
  return delta;
}

long aver_sens()
{
  pressure = 0;
  for (byte i = 0; i < 10; i++)
  {
    pressure += bmp.readPressure();
  }
  aver_pressure = pressure / 10;
  return aver_pressure;
}
