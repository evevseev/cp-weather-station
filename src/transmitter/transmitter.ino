#include <Gyver433.h>
#include <GyverPower.h>
#include <util/delay.h>
#include <microDS18B20.h>

// Настройки передатчика
#define SLEEP_TIME 10000
#define SENSOR_ID 0x01

// Параметры передачи
#define G433_SPEED 1000
#define RADIO_BUF_SIZE 5

// Пины
#define RADIO_DATA PB0
#define RADIO_VCC PB3

#define DS_DATA PB1
#define DS_VCC PB2

MicroDS18B20<DS_DATA> ds;
Gyver433_TX<RADIO_DATA, RADIO_BUF_SIZE, G433_XOR> tx;

void setup()
{
  pinMode(RADIO_VCC, OUTPUT);
  pinMode(DS_VCC, OUTPUT);

  // Настраиваем deep-sleep
  power.hardwareDisable(PWR_ALL);
  power.setSleepMode(POWERDOWN_SLEEP);
}

void loop()
{
  // Отправляем запрос на чтение
  digitalWrite(DS_VCC, HIGH);
  _delay_ms(1);
  ds.requestTemp();
  power.sleep(SLEEP_1024MS);

  // Подготавливаекм данные к отправке
  uint16_t result = ds.getRaw();

  uint8_t data[4];
  data[0] = SENSOR_ID;        // ID
  data[1] = 0xA5;             // SENSOR_TYPE
  data[2] = highByte(result); // Первые 8 бит RESULT
  data[3] = lowByte(result);  // Вторые 8 бит RESULT

  // Отправляем
  digitalWrite(RADIO_VCC, HIGH);
  _delay_ms(1);
  tx.sendData(data);
  digitalWrite(RADIO_VCC, LOW);

  // Выключаем и спим
  digitalWrite(DS_VCC, LOW);
  power.sleepDelay(SLEEP_TIME);
}
