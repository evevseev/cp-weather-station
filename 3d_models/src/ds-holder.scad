$fn = 100;

difference() {
    cylinder(r=45 / 2, h=10, center=true);
    cylinder(r=40 / 2, h=15, center=true);
}

difference() {
    cylinder(r=9 / 2, h=10, center=true);
    cylinder(r=5.8 / 2, h=15, center=true);
}

difference() {
        for (i=[1:2]) {
            rotate([0, 0, 360 / 4 * i]) cube(size=[40, 4, 10], center=true);
        }
    cylinder(r=9 / 2, h=10, center=true);
    
}

