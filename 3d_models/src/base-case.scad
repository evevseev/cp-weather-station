wall_thickness = 3;
$fn = 100;

intersection() {
    difference() {
        union() {
            difference() {
                cube(size=[50, 50, 50], center=true);
                cube(size=[50 - wall_thickness, 50 - wall_thickness, 50 - wall_thickness], center=true);
            }

            translate([-22 + wall_thickness, -18 - wall_thickness, 18 + wall_thickness]) rotate([0, -90, 0])  screw_hole();
            translate([-22 + wall_thickness, 18 + wall_thickness, 18 + wall_thickness]) rotate([0, -90, 0])  screw_hole();
            translate([-22 + wall_thickness, -18 - wall_thickness, -18 - wall_thickness]) rotate([0, -90, 0])  screw_hole();
            translate([-22 + wall_thickness, 18 + wall_thickness, -18 - wall_thickness]) rotate([0, -90, 0])  screw_hole();
        }
        
        union() {
            // Back Lid

            // translate([-25, 0, 0]) cube(size=[wall_thickness + 1, 50, 50], center=true); 

            translate([25, 0, 0])  cube(size=[wall_thickness, 27, 20], center=true);
            translate([-25, 0, -25 + 2 * wall_thickness])  cube(size=[wall_thickness, 7, 3], center=true);
            translate([-25, 0, 0])  cube(size=[wall_thickness, 10, 2.5], center=true);

            translate([-25, -21, -21]) rotate([0, -90, 0]) cylinder(r=4.6 / 2, h=wall_thickness, center=true);
            translate([-25, +21, -21]) rotate([0, -90, 0]) cylinder(r=4.6 / 2, h=wall_thickness, center=true);
            translate([-25, -21, 21]) rotate([0, -90, 0]) cylinder(r=4.6 / 2, h=wall_thickness, center=true);
            translate([-25, +21, 21]) rotate([0, -90, 0]) cylinder(r=4.6 / 2, h=wall_thickness, center=true);

        }
    }
     // Back Lid
     
   // translate([-25, 0, 0]) cube(size=[wall_thickness, 50, 50], center=true); 
}


module screw_hole() {
    difference() {
        cube(size=[6, 6, 8], center=true);
        translate([0, 0, 1]) cylinder(r=3.4 / 2, h=6, center=true);
    }
}